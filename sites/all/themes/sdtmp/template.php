<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * APH theme.
 */

function sdtmp_form_alter(&$form, &$form_state, $form_id) {
    /*if ($form_id == 'webform_client_form_42') {
        /*$form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
        $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
        $form['search_block_form']['#size'] = 30; // define size of the textfield
        //$form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield
        //$form['actions']['submit']['#value'] = t('GO!'); // Change the text on the submit button
        $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button-32.png');
        // Add extra attributes to the text box
        //$form['search_block_form']['#attributes']['onblur'] = 'if (this.value == '') {this.value = 'Search';}';
        //$form['search_block_form']['#attributes']['onfocus'] = 'if (this.value == 'Search') {this.value = '';}';
        // Prevent user from searching the default text
        //$form['#attributes']['onsubmit'] = 'if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }';
        // Alternative (HTML5) placeholder attribute instead of using the javascript
        $form['webform_client_form_42']['#attributes']['placeholder'] = t('Search...');

        dsm($form);
    }*/
    if (isset($form['submitted'] )) {
        foreach ($form['submitted'] as $key => $value) {
            if ('textfield' == $value['#type']) {
                $form['submitted'][$key]['#attributes']['placeholder'] = t($value['#title']);
                $form['submitted'][$key]['#title'] = '';
            }
            if($value['#type'] == 'webform_email') {
                $form['submitted'][$key]['#attributes']['placeholder'] = t($value['#title']);
                $form['submitted'][$key]['#title'] = '';
            }
            if($value['#type'] == 'textarea') {
                $form['submitted'][$key]['#attributes']['placeholder'] = t($value['#title']);
                $form['submitted'][$key]['#title'] = '';

            }
        }
    }
}

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */
function sdtmp_breadcrumb($variables) {
    $breadcrumb = $variables['breadcrumb'];

    // MB: Check the type of page and the following code will run to set the breadcrumb
    $type = arg(0);
    $taxType = arg(1);

    if($type == 'node'){

        //Get the node type by loading the node id into the node object and accessing it as follows (nid is loacted at arg(1) for a node)
        $node = node_load(arg(1));
        $nodeType = $node->type;

        if($nodeType == "blog_post"){

            //Find the blog category by searching the three parameters. node type, entity, and field name (machine name)
            $blog_category = field_get_items('node', $node, 'field_blog_category');

            //give new variable the value of the blog category ID
            $tid = $blog_category[0]['tid'];

            //Load the tax term id.
            $cat_term = taxonomy_term_load($tid);

            //"convert" tax term id to tax name.
            $tax = $cat_term->name;
            $tax2 = str_replace(" ","-",$tax);

            $breadcrumb[1] = '<a href="/blog">' . 'Blog' . '</a>';
            $breadcrumb[2] = '<a href="../' . strtolower($tax2) . '">' . ucfirst($tax) .'</a>' ;
            $breadcrumb[3] = $node->title;
        }

        else if($nodeType == "article"){
            $breadcrumb[1] = $node->title;
        }

        else if($nodeType == "contact_page"){
            $breadcrumb[1] = $node->title;
        }

        else if($nodeType == "event"){
            $breadcrumb[1] = '<a href="/events">Events</a>';
            $breadcrumb[2] = $node->title;
        }

        else if($nodeType == "service"){
            $breadcrumb[1] = 'Services';
            $breadcrumb[2] = $node->title;
        }

        else if($nodeType == "page"){
            $breadcrumb[1] = $node->title;
        }

        else if($nodeType == "our_work"){
            $breadcrumb[1] = '<a href="/our-work">' . 'Our Work' . '</a>';
            $breadcrumb[2] = $node->title;
        }

        else if($nodeType == "webform"){
            $breadcrumb[1] = $node->title;
        }

    }

    if($type == 'blog'){
        $breadcrumb[1] = "Blog";
    }

    if($type == 'events'){
        $breadcrumb[1] = "Events";
    }

    if($type == 'gallery'){
        $breadcrumb[1] = "Gallery";
    }

    if($type == 'our-work'){
        $breadcrumb[1] = "Our Work";
    }

    if($type == 'testimonials'){
        $breadcrumb[1] = "Testimonials";
    }

    if($type == 'our-services'){
        $breadcrumb[1] = "Our Services";
    }
    $blogcategory = arg(2);
    if($taxType == 'category'){

        $breadcrumb[1] = '<a href="/blog">' . 'Blog' . '</a>';
        $breadcrumb[2] = ucfirst($blogcategory);
    }

    if (!empty($breadcrumb)) {
        // Provide a navigational heading to give context for breadcrumb links to
        // screen-reader users. Make the heading invisible with .element-invisible.
        $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

        $output .= '<div class="breadcrumb">' . implode(' > ', $breadcrumb) . '</div>';
        return $output;
    }

}