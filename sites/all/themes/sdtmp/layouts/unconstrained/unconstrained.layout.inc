name = unconstrained
description = SilverDisc Unconstrained layout
preview = preview.png
template = unconstrained-layout

; Regions
regions[branding]                   = Branding
regions[header]                     = Header
regions[navigation]                 = Navigation bar
regions[secondary_navigation]       = Navigation Secondary
regions[slideshow]                  = Slideshow
regions[highlighted]                = Highlighted
regions[help]                       = Help
regions[content_first]              = Content First
regions[content]                    = Content
regions[sidebar_first]              = First sidebar
regions[sidebar_second]             = Second sidebar
regions[product_display]            = Product display
regions[blog_display]               = Blog display
regions[newsletter_display]         = Newsletter display
regions[footer_first]               = Footer first
regions[footer]                     = Footer

; Stylesheets
stylesheets[all][] = css/layouts/unconstrained/unconstrained.layout.css


breakpoints[mobile] = (min-width: 0px)
breakpoints[narrow] = (min-width: 740px)
breakpoints[normal] = (min-width: 980px)
breakpoints[wide] = (min-width: 1220px)
